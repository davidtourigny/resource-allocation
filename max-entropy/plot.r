######## R script for plotting example 5 #########

files <- list.files(path="results/vertices", pattern="*.txt", full.names=TRUE, recursive=FALSE)

vertex_plot <- function(x){
    
    X = as.matrix(read.table(x))
    t = X[,1]
    B = X[,2]
    G = X[,3]
    E = X[,4]
    O = 0.001*X[,5]
    C = X[,6]
    
    D = 0.0
    kLa = 30.0
    Vmax1 = 1.0
    Vmax2 = 1.0
    Vmax3 = 1.0
    Vmax4 = 1.0
    Vmax5 = 1.0
    Vmax6 = 2.0
    c1 = 0.02
    c3 = 0.34
    K1 = 0.01
    K2 = 0.01
    K3 = 0.01
    K4 = 0.01
    K5 = 0.01
    K6 = 0.01
    KO2 = 0.001
    KO3 = 0.001
    KO5 = 0.001
    Go = 10.0
    Ostar = 0.015
    sigma = 1.0
    
    r1 = Vmax1*G/(K1 + G)
    r2 = Vmax2*E*O/((K2 + E)*(KO2 + O))
    r3 = Vmax3*G*O/((K3 + G)*(KO3 + O))
    r4 = Vmax4*C/(K4 + C)
    r5 = Vmax5*C*O/((K5 + C)*(KO5 + O))
    r6 = Vmax6*G/(K6 + G)
    
    Y1 = c1/2
    Y2 = c3
    Y3 = (c1 + 2.0*c3)/2
    
    er1 = exp(r1*B*Y1/sigma)
    er2 = exp(r2*B*Y2/sigma)
    er3 = exp(r3*B*Y3/sigma)
    er4 = exp(r4*B*Y1/sigma)
    er5 = exp(r5*B*Y3/sigma)
    er6 = exp(0.0/sigma)
    
    ersum = er1 + er2 + er3 + er4 + er5 + er6
    
    u1 = er1/ersum
    u2 = er2/ersum
    u3 = er3/ersum
    u4 = er4/ersum
    u5 = er5/ersum
    u6 = er6/ersum
    
    R1 = r1*u1
    R2 = r2*u2
    R3 = r3*u3
    R4 = r4*u4
    R5 = r5*u5
    R6 = r6*u6
    
    sumrv = R1*Y1 + R2*Y2 + R3*Y3 + R4*Y1 + R5*Y3
    
    ymax = max(X[,2:5])
    ymin = min(X[,2:5])
    
    filename = paste(sub("*.txt", "",x),".pdf", sep = "")
    pdf(file=filename)
    plot(t,B,type="l",ylim=c(0,10),xlab='',ylab='',bty="l") # x
    lines(t,G,lty=2,col="black")  # G
    lines(t,E,lty=3,col="black") # E
    lines(t,C,lty=1,lwd=2,col="black") # C
    title(xlab="Time (h)",ylab="Concentration (g/L)",cex.sub=0.299)
    legend(x=30,y=10, legend=c("Catalytic biomass (x)", "Glucose (G)", "Ethanol (E)","Stored carbohydrate (C)"),col="black",lty=c(1,2,3,1),lwd=c(1,1,1,2),bty="n")
    
    filename = paste(sub("*.txt", "",x),"_u.pdf", sep = "")
    pdf(file=filename)
    plot(t,u1,type="l",ylim=c(0,1),xlab='',ylab='') # u1
    lines(t,u2,col="green")  # u2
    lines(t,u3,col="blue") # u3
    lines(t,u4,col="red") # u4
    lines(t,u5,col="violet") # u5
    lines(t,u6,col="orange") # u6
    title(main="cell",xlab="time",ylab="output",cex.sub=0.299)
    legend(x=0,y=1, legend=c("u1", "u2", "u3", "u4","u5", "u6"),col=c("black","green","blue","red","violet","orange"),lty=1,bty="n")
    
    
    filename = paste(sub("*.txt", "",x),"_r.pdf", sep = "")
    pdf(file=filename)
    rmax = max(r1,r2,r3,r4,r5,r6)
    plot(t,r1,type="l",ylim=c(0,rmax),xlab='',ylab='') # u1
    lines(t,r2,col="green")  # u2
    lines(t,r3,col="blue") # u3
    lines(t,r4,col="red") # u4
    lines(t,r5,col="violet") # u5
    lines(t,r6,col="orange") # u6
    title(main="cell",xlab="time",ylab="output",cex.sub=0.299)
    legend(x=0,y=1, legend=c("r1", "r2", "r3", "r4","r5", "r6"),col=c("black","green","blue","red","violet","orange"),lty=1,bty="n")
    dev.off()
}

lapply(files, vertex_plot)
