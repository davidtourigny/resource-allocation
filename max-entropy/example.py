# Copyright (C) David S. Tourigny 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# import modules
from ccss.network import VertexValue, PyGraph
from ccss.functions import Function, VertexFunction
import numpy as np
import sympy

# create and define vertex function "Yeast" with 5 dimensions and 18 parameters
vertex_function = VertexFunction("Yeast", 5, 22)
X = vertex_function.get_vars()
P = vertex_function.get_params()

D = P[0]
kLa = P[1]
Vmax1 = P[2]
Vmax2 = P[3]
Vmax3 = P[4]
Vmax4 = P[5]
Vmax5 = P[6]
Vmax6 = P[7]
c1 = P[8]
c3 = P[9]
K1 = P[10]
K2 = P[11]
K3 = P[12]
K4 = P[13]
K5 = P[14]
K6 = P[15]
KO2 = P[16]
KO3 = P[17]
KO5 = P[18]
Go = P[19]
Ostar = P[20]
sigma = P[21]

t = X[0]
x = X[1]
G = X[2]
E = X[3]
O = 0.001*X[4]
C = X[5]
CC = C/x

r1 = Vmax1*G/(K1 + G)
r2 = Vmax2*E*O/((K2 + E)*(KO2 + O))
r3 = Vmax3*G*O/((K3 + G)*(KO3 + O))
r4 = Vmax4*CC/(K4 + CC)
r5 = Vmax5*CC*O/((K5 + CC)*(KO5 + O))
r6 = Vmax6*G/(K6 + G)

Y1 = c1*0.5
Y2 = c3
Y3 = (c1 + 2.0*c3)*0.5

er1 = sympy.exp((r1*Y1)*x/sigma)
er2 = sympy.exp((r2*Y2)*x/sigma)
er3 = sympy.exp((r3*Y3)*x/sigma)
er4 = sympy.exp((r4*Y1)*x/sigma)
er5 = sympy.exp((r5*Y3)*x/sigma)
er6 = sympy.exp(0.0/sigma)

ersum = er1 + er2 + er3 + er4 + er5 + er6

u1 = er1/ersum
u2 = er2/ersum
u3 = er3/ersum
u4 = er4/ersum
u5 = er5/ersum
u6 = er6/ersum

R1 = r1*u1
R2 = r2*u2
R3 = r3*u3
R4 = r4*u4
R5 = r5*u5
R6 = r6*u6

sumrv = R1*Y1 + R2*Y2 + R3*Y3 + R4*Y1 + R5*Y3

vertex_function.set_expression(1, (sumrv - D)*x )
vertex_function.set_expression(2, (Go - G)*D - 0.5*(R1 + R3 + R6)*x )
vertex_function.set_expression(4, kLa*(Ostar - O) - (R2 + R3 + R5)*x )
vertex_function.set_expression(3, -E*D + (R1 - R2 + R4)*x )
vertex_function.set_expression(5, -C*D + 0.5*(R6 - R4 - R5)*x )


# create PyGraph object with name "example5"
network = PyGraph("example5")

# create vertex values and set function (optional methods gen_inits and gen_params to be used for random number generation)
inits = [0.1, 0.04, 0.0, 0.01, 0.0]
params = [0.135, 150.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.5, 0.02, 0.34, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.001, 0.001, 0.001, 10.0, 0.015, 1.0]
gen_inits = lambda : np.multiply(inits,(1 + (0.1 - np.random.rand(5)/4))).tolist()
gen_params = lambda : np.multiply(params,(1 + (0.1 - np.random.rand(18)/5)/5)).tolist()

for i in range(1) :
    vertex_value = VertexValue()
    vertex_value.set_function(vertex_function)
    network.add_vertex("cell" + str(i+1))
    vertex_value.set_init_conds(inits) #can use gen_inits here for randomized intital conditions
    vertex_value.set_params(params) #can use gen_params here for randomized parameter values
    network.set_vertex_value("cell" + str(i+1), vertex_value)

# run simulation over interval [0.0, 60.0] with outputs every 0.1
network.rel_tolerance = 1e-8 # change rel_tolerance attribute to 10e-8
network.abs_tolerance = 1e-8 # change abs_tolerance attribute to 10e-8
network.simulate(0.0, 400.0, 0.1)
