## *CCSS* example from Section 6 of [arXiv:1906.03919](https://arxiv.org/abs/1906.03919)

This directory contains the following content:

* [`./example.py`](./example.py): *Python* script for *CCSS* example
* [`./plot.r`](./plot.r): *R* script for plotting output of example
* [`./README.md`](./README.md): this document

