# import modules
from ccss_network import *
from ccss_functions import *
import numpy as np

# create and define vertex function "MultiCell" with 8 dimensions and 19 parameters
vertex_function = VertexFunction("MultiCell", 8, 19)
X = vertex_function.get_vars()
P = vertex_function.get_params()

# same parameters for both nodes
D = P[0]
kLa = P[1]
Vmax1 = P[2]
Vmax2 = P[3]
Vmax3 = P[4]
c1 = P[5]
c3 = P[6]
K1 = P[7]
K2 = P[8]
K3 = P[9]
KO2 = P[10]
KO3 = P[11]
Go = P[12]
Ostar = P[13]
sigma = P[14]
DG = P[15]
DL = P[16]
DO = P[17]
p = P[18]

# time variable
t = X[0]

# variables for node 1
x1 = X[1]
G1 = X[2]
L1 = X[3]
O1 = 0.001*X[4]

# variables for node 2
x2 = X[5]
G2 = X[6]
L2 = X[7]
O2 = 0.001*X[8]

# fluxes for node 1
r11 = Vmax1*G1/(K1 + G1)
r21 = Vmax2*L1*O1/((K2 + L1)*(KO2 + O1))
r31 = Vmax3*G1*O1/((K3 + G1)*(KO3 + O1))

# fluxes for node 2
r12 = Vmax1*G2/(K1 + G2)
r22 = Vmax2*L2*O2/((K2 + L2)*(KO2 + O2))
r32 = Vmax3*G2*O2/((K3 + G2)*(KO3 + O2))

# same coefficients for both nodes
Y1 = c1*0.5
Y2 = c3
Y3 = (c1 + 2.0*c3)*0.5

# generalised mean
M = x1**p + x2**p
inv_p = p**(-1)
Mp = (0.5*M)**inv_p

# sigmoid for node 1
sigmoid1 = (x1**p)/M

# sigmoid for node 2
sigmoid2 = (x2**p)/M

# resource allocation for node 1
er11 = sympy.exp((r11*Y1)*sigmoid1*Mp/sigma)
er21 = sympy.exp((r21*Y2)*sigmoid1*Mp/sigma)
er31 = sympy.exp((r31*Y3)*sigmoid1*Mp/sigma)

ersum1 = er11 + er21 + er31

u11 = er11/ersum1
u21 = er21/ersum1
u31 = er31/ersum1

R11 = r11*u11
R21 = r21*u21
R31 = r31*u31

sumrv1 = R11*Y1 + R21*Y2 + R31*Y3

# resource allocation for node 2
er12 = sympy.exp((r12*Y1)*sigmoid2*Mp/sigma)
er22 = sympy.exp((r22*Y2)*sigmoid2*Mp/sigma)
er32 = sympy.exp((r32*Y3)*sigmoid2*Mp/sigma)

ersum2 = er12 + er22 + er32

u12 = er12/ersum2
u22 = er22/ersum2
u32 = er32/ersum2

R12 = r12*u12
R22 = r22*u22
R32 = r32*u32

sumrv2 = R12*Y1 + R22*Y2 + R32*Y3

# dynamics for node 1
vertex_function.set_expression(1, (sumrv1 - D)*x1 )
vertex_function.set_expression(2, (Go - G1)*D - 0.5*(R11 + R31)*x1 + DG*(G2 - G1) )
vertex_function.set_expression(4, kLa*(Ostar - O1) - (R21 + R31)*x1 + DO*(O2 - O1) )
vertex_function.set_expression(3, -L1*D + (R11 - R21)*x1 + DL*(L2 - L1) )

# dynamics for node 2
vertex_function.set_expression(5, (sumrv2 - D)*x2 )
vertex_function.set_expression(6, (Go - G2)*D - 0.5*(R12 + R32)*x2 + DG*(G1 - G2) )
vertex_function.set_expression(8, kLa*(Ostar - O2) - (R22 + R32)*x2 + DO*(O1 - O2) )
vertex_function.set_expression(7, -L2*D + (R12 - R22)*x2 + DL*(L1 - L2) )


# create PyGraph object with name "example6"
network = PyGraph("example6")

# create vertex values and set function (optional methods gen_inits and gen_params to be used for random number generation)
inits = [0.1, 10.0, 0.0, 0.01,
         0.1, 0.0, 0.0, 0.01]

params = [0.0, 30.0, 1.0, 1.0, 1.0, 0.02, 0.34, 0.01, 0.01, 0.01, 0.001, 0.001, 0.0, 0.015, 1.0, 0.0, 0.5, 10.0, 100.0]

gen_inits = lambda : np.multiply(inits,(1 + (0.1 - np.random.rand(4)/4))).tolist()
gen_params = lambda : np.multiply(params,(1 + (0.1 - np.random.rand(15)/5)/5)).tolist()

for i in range(1) :
    vertex_value = VertexValue()
    vertex_value.set_function(vertex_function)
    network.add_vertex("cell" + str(i+1))
    vertex_value.set_init_conds(inits) #can use gen_inits here for randomized intital conditions
    vertex_value.set_params(params) #can use gen_params here for randomized parameter values
    network.set_vertex_value("cell" + str(i+1), vertex_value)

# run simulation over interval [0.0, 60.0] with outputs every 0.1
network.rel_tolerance = 1e-8 # change rel_tolerance attribute to 10e-8
network.abs_tolerance = 1e-8 # change abs_tolerance attribute to 10e-8
network.simulate(0.0, 60.0, 0.1)
