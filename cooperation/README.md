## *CCSS* example from Section 4 of [arXiv:1908.05307](https://arxiv.org/abs/1908.05307)

This directory contains the following content:

* [`./example.py`](./example.py): *Python* script for *CCSS* example
* [`./plot.r`](./plot.r): *R* script for plotting output of example
* [`./README.md`](./README.md): this document

