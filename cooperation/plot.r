######## R script for plotting example #########

files <- list.files(path="results/vertices", pattern="*.txt", full.names=TRUE, recursive=FALSE)

vertex_plot <- function(x){
    
    X = as.matrix(read.table(x))
    t = X[,1]
    B1 = X[,2]
    G1 = X[,3]
    L1 = X[,4]
    
    B2 = X[,6]
    G2 = X[,7]
    L2 = X[,8]
    
    ymax1 = max(X[,2:4])
    ymin1 = min(X[,2:4])
    
    ymax2 = max(X[,6:8])
    ymin2 = min(X[,6:8])
    
    filename1 = paste(sub("*.txt", "1",x),".pdf", sep = "")
    pdf(file=filename1)
    plot(t,B1,type="l",ylim=c(ymin1,ymax1),xlab='',ylab='',bty="l") # x
    lines(t,G1,lty=2,col="black")  # G
    lines(t,L1,lty=3,col="black") # E
    title(xlab="Time (h)",ylab="Concentration (g/L)",cex.sub=0.299)
    legend(x=100,y=10, legend=c("Catalytic biomass (x)", "Glucose (G)", "Lactate (L)"),col="black",lty=c(1,2,3),lwd=c(1,1,1),bty="n")
    dev.off()
    
    filename2 = paste(sub("*.txt", "2",x),".pdf", sep = "")
    pdf(file=filename2)
    plot(t,B2,type="l",ylim=c(ymin2,ymax2),xlab='',ylab='',bty="l") # x
    lines(t,G2,lty=2,col="black")  # G
    lines(t,L2,lty=3,col="black") # E
    title(xlab="Time (h)",ylab="Concentration (g/L)",cex.sub=0.299)
    legend(x=100,y=10, legend=c("Catalytic biomass (x)", "Glucose (G)", "Lactate (L)"),col="black",lty=c(1,2,3),lwd=c(1,1,1),bty="n")
    dev.off()
    
}

lapply(files, vertex_plot)
