# Dynamic and Cooperative Metabolic Resource Allocation Based on the Maximum Entropy Principle

This repository contains code for reproducing figures from the author's papers on dynamic metabolic resource allocation:
* The directory [max-entropy](./max-entropy/) contains code to reproduce the example model described in Section 6 of [arXiv:1906.03919](https://arxiv.org/abs/1906.03919) (now published [J. Math. Biol. 2020; 80:2395-2430](https://link.springer.com/article/10.1007/s00285-020-01499-6))
* The directory [cooperation](./cooperation/) contains code to reproduce the example model described in Section 4 of [arXiv:1908.05307](https://arxiv.org/abs/1908.05307) (under review in J. Math. Biol.)

## Dependencies

Running the examples depends on the *Python* package [*CCSS*](https://gitlab.com/davidtourigny/coupled-cell-systems), also written by the author. To reproduce figures from the corresponding papers one can use the provided scripts written in the programming language [R](https://www.r-project.org/).
